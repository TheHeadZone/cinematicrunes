﻿Shader "MyShader/TextureShader" {
	Properties {
		_MainTex ("Main (RGB)", 2D) = "white" {}
		_OtherTex ("Other ", 2D) = "white" {}
		_Alpha ("Alpha", Range(0, 10)) = 0
		_Tint("Tint", Color) = (0, 0 ,0, 0)
	}
	SubShader  {

		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
		#pragma surface surf Standard

		sampler2D _MainTex;
		sampler2D _OtherTex;
		float _Alpha;
		fixed4 _Tint;


		struct Input {
			float2 uv_MainTex;
			float2 uv_OtherTex;
		};

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
			fixed4 d = tex2D(_OtherTex, IN.uv_OtherTex + sin(_Time * 0.5));
			fixed4 final = lerp(c, d, sin(_Time * _Alpha));
			final = final * _Tint;

			o.Albedo = final.rgb;
			o.Alpha = final.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
